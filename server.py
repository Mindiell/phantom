# encoding: utf-8
"""
Server module.

Initializes Flask application and extensions and runs it.
"""

from datetime import datetime, timedelta
import json
import locale
import os

from flask import Flask, Response, request, render_template
from flask_cors import CORS


locale.setlocale(locale.LC_ALL, "fr_FR.UTF-8")
application = Flask(__name__)
application.config.from_object("settings")
CORS(application)

UPDATE_ACTIVITIES = 4
UPDATE_SUBSCRIPTIONS = 1


def search_structure_by_activite(activite_id):
    filepath_structures = os.path.join(application.config["DATA_PATH"], "structures")
    for structure in os.listdir(filepath_structures):
        filepath_activites = os.path.join(
            application.config["DATA_PATH"],
            "activites",
            structure,
        )
        if activite_id in os.listdir(filepath_activites):
            return structure


def search_structure_by_seance(seance_id):
    filepath_structures = os.path.join(application.config["DATA_PATH"], "structures")
    for structure in os.listdir(filepath_structures):
        filepath_seances = os.path.join(
            application.config["DATA_PATH"],
            "seances",
            structure,
        )
        if seance_id in os.listdir(filepath_seances):
            return structure


def load_updates():
    # On charge les informations de mise à jour
    updates = {
        "departments": None,
        "structures": None,
        "activites": None,
        "seances": None,
        "inscriptions": None,
        "utilisateurs": None,
        "activites_loges": None,
        "seances_loges": None,
        "inscriptions_loges": None,
        "activites_jop": None,
        "seances_jop": None,
        "inscriptions_jop": None,
    }
    try:
        with open(
            os.path.join(application.config["DATA_PATH"], "updates.json")
        ) as file_handler:
            updates.update(json.load(file_handler))
    except FileNotFoundError:
        pass

    return updates


def save_updates(what):
    # On sauve les informations de mise à jour
    updates = load_updates()
    updates[what] = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
    with open(
        os.path.join(application.config["DATA_PATH"], "updates.json"),
        "w",
    ) as file_handler:
        json.dump(updates, file_handler, indent=2)


def what_to_update():
    whats = {
        "departments": [],
        "structures": [],
        "activites": [],
        "seances": [],
        "inscriptions": [],
        "utilisateurs": [],
    }
    filepath_structures = os.path.join(application.config["DATA_PATH"], "structures")
    filepath_activites = os.path.join(application.config["DATA_PATH"], "activites")
    filepath_inscriptions = os.path.join(application.config["DATA_PATH"], "inscriptions")
    filepath_utilisateurs = os.path.join(application.config["DATA_PATH"], "utilisateurs")
    filepath_inscriptions_loges = os.path.join(application.config["DATA_PATH"], "loges", "inscriptions")
    filepath_inscriptions_jop = os.path.join(application.config["DATA_PATH"], "jop", "inscriptions")
    updates = load_updates()

    if updates["departments"] is None:
        whats["departments"].append("78")

    if updates["structures"] is None:
        for structure in os.listdir(filepath_structures):
            whats["structures"].append(structure)

    update_activites = updates["activites"] is None
    if not update_activites:
        last_update = datetime.strptime(updates["activites"], "%Y-%m-%d %H:%M:%S")
        if last_update + timedelta(hours=UPDATE_ACTIVITIES) < datetime.now():
            update_activites = True
    if update_activites:
        for structure in os.listdir(filepath_structures):
            whats["activites"].append(structure)

    update_seances = updates["seances"] is None
    if not update_seances:
        last_update = datetime.strptime(updates["seances"], "%Y-%m-%d %H:%M:%S")
        if last_update + timedelta(hours=UPDATE_ACTIVITIES) < datetime.now():
            update_seances = True
    if update_seances:
        for structure in os.listdir(filepath_activites):
            # On cherche les séances à mettre à jour pour chaque activité
            filepath_seances = os.path.join(filepath_activites, structure)
            for activite in os.listdir(filepath_seances):
                filename_activite = os.path.join(filepath_seances, activite)
                with open(filename_activite) as file_handler:
                    activite = json.load(file_handler)
                    for seance in activite["seanceList"]:
                        whats["seances"].append(seance["id"])

    update_inscriptions = updates["inscriptions"] is None
    if not update_inscriptions:
        last_update = datetime.strptime(updates["inscriptions"], "%Y-%m-%d %H:%M:%S")
        if last_update + timedelta(hours=UPDATE_SUBSCRIPTIONS) < datetime.now():
            update_inscriptions = True
    if update_inscriptions:
        for structure in os.listdir(filepath_activites):
            # On cherche les inscriptions à mettre à jour pour chaque séance
            filepath_seances = os.path.join(filepath_activites, structure)
            for activite in os.listdir(filepath_seances):
                filename_activite = os.path.join(filepath_seances, activite)
                with open(filename_activite) as file_handler:
                    activite = json.load(file_handler)
                    for seance in activite["seanceList"]:
                        whats["inscriptions"].append(seance["id"])

    # On cherche toutes les inscriptions
    for structure in os.listdir(filepath_inscriptions):
        list_inscriptions = os.path.join(filepath_inscriptions, structure)
        for file_inscription in os.listdir(list_inscriptions):
            filename_inscription = os.path.join(list_inscriptions, file_inscription)
            with open(filename_inscription) as file_handler:
                inscription = json.load(file_handler)
                for utilisateur in inscription:
                    whats["utilisateurs"].append(utilisateur["utilisateur"]["id"])
        # On cherche toutes les inscriptions pour les loges
        for file_inscription in os.listdir(filepath_inscriptions_loges):
            filename_inscription = os.path.join(filepath_inscriptions_loges, file_inscription)
            with open(filename_inscription) as file_handler:
                inscription = json.load(file_handler)
                for utilisateur in inscription:
                    whats["utilisateurs"].append(utilisateur["utilisateur"]["id"])
        # On cherche toutes les inscriptions pour les JOP
        for file_inscription in os.listdir(filepath_inscriptions_jop):
            filename_inscription = os.path.join(filepath_inscriptions_jop, file_inscription)
            with open(filename_inscription) as file_handler:
                inscription = json.load(file_handler)
                for utilisateur in inscription:
                    whats["utilisateurs"].append(utilisateur["utilisateur"]["id"])

    # On simplifie les utilisateurs
    whats["utilisateurs"] = list(set(whats["utilisateurs"]))
    # On supprime les utilisateurs déjà connus
    for utilisateur in os.listdir(filepath_utilisateurs):
        if utilisateur in whats["utilisateurs"]:
            whats["utilisateurs"].remove(utilisateur)

    return whats


def what_loges_to_update():
    whats = {
        "activites_loges": [],
        "seances_loges": [],
        "inscriptions_loges": [],
    }
    filepath_activites_loges = os.path.join(application.config["DATA_PATH"], "loges", "activites")
    filepath_inscriptions_loges = os.path.join(application.config["DATA_PATH"], "loges", "inscriptions")
    updates = load_updates()

    update_activites_loges = updates["activites_loges"] is None
    if not update_activites_loges:
        last_update = datetime.strptime(updates["activites_loges"], "%Y-%m-%d %H:%M:%S")
        if last_update + timedelta(hours=UPDATE_ACTIVITIES) < datetime.now():
            update_activites_loges = True
    if update_activites_loges:
        whats["activites_loges"].append("loges")

    update_seances_loges = updates["seances_loges"] is None
    if not update_seances_loges:
        last_update = datetime.strptime(updates["seances_loges"], "%Y-%m-%d %H:%M:%S")
        if last_update + timedelta(hours=UPDATE_ACTIVITIES) < datetime.now():
            update_seances_loges = True
    if update_seances_loges:
        # On cherche les séances à mettre à jour pour chaque activité
        for activite_loges in os.listdir(filepath_activites_loges):
            filename_activite_loges = os.path.join(filepath_activites_loges, activite_loges)
            with open(filename_activite_loges) as file_handler:
                activite_loges = json.load(file_handler)
                for seance_loges in activite_loges["seanceList"]:
                    whats["seances_loges"].append(seance_loges["id"])

    update_inscriptions_loges = updates["inscriptions_loges"] is None
    if not update_inscriptions_loges:
        last_update = datetime.strptime(updates["inscriptions_loges"], "%Y-%m-%d %H:%M:%S")
        if last_update + timedelta(hours=UPDATE_SUBSCRIPTIONS) < datetime.now():
            update_inscriptions_loges = True
    if update_inscriptions_loges:
        # On cherche les inscriptions à mettre à jour pour chaque séance
        for activite_loges in os.listdir(filepath_activites_loges):
            filename_activite_loges = os.path.join(filepath_activites_loges, activite_loges)
            with open(filename_activite_loges) as file_handler:
                activite_loges = json.load(file_handler)
                for seance_loges in activite_loges["seanceList"]:
                    whats["inscriptions_loges"].append(seance_loges["id"])

    return whats


def what_jop_to_update():
    whats = {
        "activites_jop": [],
        "seances_jop": [],
        "inscriptions_jop": [],
    }
    filepath_activites_jop = os.path.join(application.config["DATA_PATH"], "jop", "activites")
    filepath_inscriptions_jop = os.path.join(application.config["DATA_PATH"], "jop", "inscriptions")
    updates = load_updates()

    update_activites_jop = updates["activites_jop"] is None
    if not update_activites_jop:
        last_update = datetime.strptime(updates["activites_jop"], "%Y-%m-%d %H:%M:%S")
        if last_update + timedelta(hours=UPDATE_ACTIVITIES) < datetime.now():
            update_activites_jop = True
    if update_activites_jop:
        whats["activites_jop"].append("jop")

    update_seances_jop = updates["seances_jop"] is None
    if not update_seances_jop:
        last_update = datetime.strptime(updates["seances_jop"], "%Y-%m-%d %H:%M:%S")
        if last_update + timedelta(hours=UPDATE_ACTIVITIES) < datetime.now():
            update_seances_jop = True
    if update_seances_jop:
        # On cherche les séances à mettre à jour pour chaque activité
        for activite_jop in os.listdir(filepath_activites_jop):
            filename_activite_jop = os.path.join(filepath_activites_jop, activite_jop)
            with open(filename_activite_jop) as file_handler:
                activite_jop = json.load(file_handler)
                for seance_jop in activite_jop["seanceList"]:
                    whats["seances_jop"].append(seance_jop["id"])

    update_inscriptions_jop = updates["inscriptions_jop"] is None
    if not update_inscriptions_jop:
        last_update = datetime.strptime(updates["inscriptions_jop"], "%Y-%m-%d %H:%M:%S")
        if last_update + timedelta(hours=UPDATE_SUBSCRIPTIONS) < datetime.now():
            update_inscriptions_jop = True
    if update_inscriptions_jop:
        # On cherche les inscriptions à mettre à jour pour chaque séance
        for activite_jop in os.listdir(filepath_activites_jop):
            filename_activite_jop = os.path.join(filepath_activites_jop, activite_jop)
            with open(filename_activite_jop) as file_handler:
                activite_jop = json.load(file_handler)
                for seance_jop in activite_jop["seanceList"]:
                    whats["inscriptions_jop"].append(seance_jop["id"])

    return whats


@application.before_request
def init_folders():
    os.makedirs(application.config["DATA_PATH"], exist_ok=True)
    for name in ("structures", "seances", "activites", "inscriptions", "utilisateurs"):
        os.makedirs(os.path.join(application.config["DATA_PATH"], name), exist_ok=True)
    for name in ("seances", "activites", "inscriptions"):
        os.makedirs(os.path.join(application.config["DATA_PATH"], "loges", name), exist_ok=True)
    for name in ("seances", "activites", "inscriptions"):
        os.makedirs(os.path.join(application.config["DATA_PATH"], "jop", name), exist_ok=True)


@application.route("/")
def hello_world():
    return render_template("index.html")


@application.route("/what", methods=["OPTIONS", "GET"])
def what():
    """
    This endpoint tells user-script what to update next.
    """
    return Response(json.dumps(what_to_update()), mimetype="application/json")


@application.route("/what_loges", methods=["OPTIONS", "GET"])
def what_loges():
    """
    This endpoint tells user-script what to update next about loges.
    """
    return Response(json.dumps(what_loges_to_update()), mimetype="application/json")


@application.route("/what_jop", methods=["OPTIONS", "GET"])
def what_jop():
    """
    This endpoint tells user-script what to update next about JO.
    """
    return Response(json.dumps(what_jop_to_update()), mimetype="application/json")


@application.route("/when", methods=["OPTIONS", "GET"])
def when():
    """
    This endpoint tells user-script when last update was.
    """
    updates = load_updates()
    date = datetime.strptime(updates["inscriptions"], "%Y-%m-%d %H:%M:%S")
    date = date.strftime("%a %d %b %Y à %Hh%M")
    return Response(date)


@application.route("/update_department", methods=["POST"])
def update_department():
    """
    This endpoint receives a list of structures to create if not existing yet.
    """
    results = json.loads(request.form["datas"])
    for structure in results["structuresFilles"]:
        filename = os.path.join(
            application.config["DATA_PATH"],
            "structures",
            str(structure["id"]),
        )
        if not os.path.exists(filename):
            with open(filename, "w") as file_handler:
                json.dump(structure, file_handler)
        # On crée les répertoires nécessaires à cette structure
        for name in ("seances", "activites", "inscriptions"):
            os.makedirs(
                os.path.join(
                    application.config["DATA_PATH"], name, str(structure["id"])
                ),
                exist_ok=True,
            )

    # Tout s'est bien passé, on sauve l'information de mise à jour
    save_updates("departments")

    return "Ok"


@application.route("/update_structure", methods=["POST"])
def update_structure():
    """
    This endpoint receives details of a structure to update it.
    """
    structure = json.loads(request.form["datas"])
    filename = os.path.join(
        application.config["DATA_PATH"],
        "structures",
        str(structure["id"]),
    )
    with open(filename, "w") as file_handler:
        json.dump(structure, file_handler)

    # Tout s'est bien passé, on sauve l'information de mise à jour
    save_updates("structures")

    return "Ok"


@application.route("/update_activite", methods=["POST"])
def update_activite():
    """
    This endpoint receives activities of a structure to update it.
    """
    results = json.loads(request.form["datas"])
    for activite in results:
        filename = os.path.join(
            application.config["DATA_PATH"],
            "activites",
            str(activite["structureMenantActivite"]["id"]),
            str(activite["id"]),
        )
        with open(filename, "w") as file_handler:
            json.dump(activite, file_handler)

    # Tout s'est bien passé, on sauve l'information de mise à jour
    save_updates("activites")

    return "Ok"


@application.route("/update_seance", methods=["POST"])
def update_seance():
    """
    This endpoint receives seances of an activity to update it.
    """
    seance = json.loads(request.form["datas"])
    # Recherche de la structure possédant cette activite
    structure_id = search_structure_by_activite(seance["activite"]["id"])
    filename = os.path.join(
        application.config["DATA_PATH"],
        "seances",
        structure_id,
        str(seance["id"]),
    )
    with open(filename, "w") as file_handler:
        json.dump(seance, file_handler)

    # Tout s'est bien passé, on sauve l'information de mise à jour
    save_updates("seances")

    return "Ok"


@application.route("/update_inscription", methods=["POST"])
def update_inscription():
    """
    This endpoint receives subscriptions for a seance to update it.
    """
    inscriptions = json.loads(request.form["datas"])
    try:
        seance_id = inscriptions[0]["seance"]["id"]
    except IndexError:
        print(inscriptions)
        return "KO"
    # Recherche de la structure possédant cette seance
    structure_id = search_structure_by_seance(seance_id)
    filename = os.path.join(
        application.config["DATA_PATH"],
        "inscriptions",
        structure_id,
        str(seance_id),
    )
    with open(filename, "w") as file_handler:
        json.dump(inscriptions, file_handler)

    # Tout s'est bien passé, on sauve l'information de mise à jour
    save_updates("inscriptions")

    return "Ok"


@application.route("/update_utilisateur", methods=["POST"])
def update_utilisateur():
    """
    This endpoint receives user to update it.
    """
    utilisateur = json.loads(request.form["datas"])
    filename = os.path.join(
        application.config["DATA_PATH"],
        "utilisateurs",
        str(utilisateur["id"]),
    )
    with open(filename, "w") as file_handler:
        json.dump(utilisateur, file_handler)

    # Tout s'est bien passé, on sauve l'information de mise à jour
    save_updates("utilisateurs")

    return "Ok"


@application.route("/update_activite_loges", methods=["POST"])
def update_activite_loges():
    """
    This endpoint receives activities for loges to update it.
    """
    results = json.loads(request.form["datas"])
    for activite in results:
        filename = os.path.join(
            application.config["DATA_PATH"],
            "loges",
            "activites",
            str(activite["id"]),
        )
        with open(filename, "w") as file_handler:
            json.dump(activite, file_handler)

    # Tout s'est bien passé, on sauve l'information de mise à jour
    save_updates("activites_loges")

    return "Ok"


@application.route("/update_seance_loges", methods=["POST"])
def update_seance_loges():
    """
    This endpoint receives seances of an activity to update it.
    """
    seance = json.loads(request.form["datas"])
    filename = os.path.join(
        application.config["DATA_PATH"],
        "loges",
        "seances",
        str(seance["id"]),
    )
    with open(filename, "w") as file_handler:
        json.dump(seance, file_handler)

    # Tout s'est bien passé, on sauve l'information de mise à jour
    save_updates("seances_loges")

    return "Ok"


@application.route("/update_inscription_loges", methods=["POST"])
def update_inscription_loges():
    """
    This endpoint receives subscriptions for a seance to update it.
    """
    inscriptions = json.loads(request.form["datas"])
    try:
        seance_id = inscriptions[0]["seance"]["id"]
    except IndexError:
        print(inscriptions)
        return "KO"
    filename = os.path.join(
        application.config["DATA_PATH"],
        "loges",
        "inscriptions",
        str(seance_id),
    )
    with open(filename, "w") as file_handler:
        json.dump(inscriptions, file_handler)

    # Tout s'est bien passé, on sauve l'information de mise à jour
    save_updates("inscriptions_loges")

    return "Ok"


@application.route("/update_activite_jop", methods=["POST"])
def update_activite_jop():
    """
    This endpoint receives activities for jop to update it.
    """
    results = json.loads(request.form["datas"])
    for activite in results:
        if activite["libelle"].startswith("JO ") or activite["libelle"].startswith("JOP"):
            filename = os.path.join(
                application.config["DATA_PATH"],
                "jop",
                "activites",
                str(activite["id"]),
            )
            with open(filename, "w") as file_handler:
                json.dump(activite, file_handler)

    # Tout s'est bien passé, on sauve l'information de mise à jour
    save_updates("activites_jop")

    return "Ok"


@application.route("/update_seance_jop", methods=["POST"])
def update_seance_jop():
    """
    This endpoint receives seances of an activity to update it.
    """
    seance = json.loads(request.form["datas"])
    filename = os.path.join(
        application.config["DATA_PATH"],
        "jop",
        "seances",
        str(seance["id"]),
    )
    with open(filename, "w") as file_handler:
        json.dump(seance, file_handler)

    # Tout s'est bien passé, on sauve l'information de mise à jour
    save_updates("seances_jop")

    return "Ok"


@application.route("/update_inscription_jop", methods=["POST"])
def update_inscription_jop():
    """
    This endpoint receives subscriptions for a seance to update it.
    """
    inscriptions = json.loads(request.form["datas"])
    try:
        seance_id = inscriptions[0]["seance"]["id"]
    except IndexError:
        print(inscriptions)
        return "KO"
    filename = os.path.join(
        application.config["DATA_PATH"],
        "jop",
        "inscriptions",
        str(seance_id),
    )
    with open(filename, "w") as file_handler:
        json.dump(inscriptions, file_handler)

    # Tout s'est bien passé, on sauve l'information de mise à jour
    save_updates("inscriptions_jop")

    return "Ok"

