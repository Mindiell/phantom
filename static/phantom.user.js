// ==UserScript==
// @name        phantom-script
// @namespace   Violentmonkey Scripts
// @match       https://pegass.croix-rouge.fr/*
// @grant       none
// @version     1.6.4
// @author      Mindiell
// @description 08/01/2025
// ==/UserScript==

const VERSION = "1.6.4";
const PEGASS_URL = "https://pegass.croix-rouge.fr/crf/rest/";
const PHANTOM_URL = "https://phantom.mytipy.net/";
const TEMPS = 500;
const JOURS = 90;

const LOGES = false;
const JO = false;


function httpGet(url, callback)
{
  var xhr = new XMLHttpRequest();
  xhr.addEventListener("load", callback);
  xhr.open("GET", url);
  xhr.send();
}


function httpPost(url, callback, datas)
{
  var xhr = new XMLHttpRequest();
  xhr.addEventListener("load", callback);
  xhr.open("POST", url);
  xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhr.send("datas="+datas);
}


function display(text) {
  console.log(text);
  let bandeau_text = document.getElementById("phantom_text");
  if (bandeau_text) {
    bandeau_text.innerHTML = text;
  }
}


// Reçoit la liste des structures d'un département
function refreshDepartment(event) {
  httpPost(PHANTOM_URL+"update_department", null, this.response);
}

// Met à jour un département
function updateDepartment(obj_id) {
  display(" updating department " + obj_id);
  httpGet(PEGASS_URL+"zonegeo/departement/"+obj_id, refreshDepartment);
}

// Reçoit la liste des détails d'une structure
function refreshStructure(event) {
  httpPost(PHANTOM_URL+"update_structure", null, this.response);
}

// Met à jour une structure
function updateStructure(obj_id) {
  display(" updating structure " + obj_id);
  httpGet(PEGASS_URL+"structure/"+obj_id, refreshStructure);
}

// Reçoit la liste des détails d'une activité
function refreshActivite(event) {
  httpPost(PHANTOM_URL+"update_activite", null, this.response);
}

// Met à jour les prochaines activités d'une structure
function updateActivite(obj_id) {
  display(" updating activitie " + obj_id);
  // On ne veut qu'une partie des activités, et seulement celles de secourisme
  let debut = new Date();
  let fin = new Date();
  fin.setDate(debut.getDate() + JOURS);
  let param_debut = debut.toISO8601().slice(0, 10);
  let param_fin = fin.toISO8601().slice(0, 10);
  httpGet(PEGASS_URL+"activite?structure="+obj_id+"&groupeAction=1&debut="+param_debut+"&fin="+param_fin, refreshActivite);
}

// Reçoit la liste des détails d'une séance
function refreshSeance(event) {
  httpPost(PHANTOM_URL+"update_seance", null, this.response);
}

// Met à jour les prochaines séances d'une activité
function updateSeance(obj_id) {
  display(" updating seance " + obj_id);
  httpGet(PEGASS_URL+"seance/"+obj_id, refreshSeance);
}

// Reçoit la liste des détails des inscriptions à une séance
function refreshInscription(event) {
  httpPost(PHANTOM_URL+"update_inscription", null, this.response);
}

// Met à jour les inscriptions à une séance
function updateInscription(obj_id) {
  display(" updating subscription " + obj_id);
  httpGet(PEGASS_URL+"seance/"+obj_id+"/inscription", refreshInscription);
}

// Reçoit la liste des détails des utilisateurs
function refreshUtilisateur(event) {
  httpPost(PHANTOM_URL+"update_utilisateur", null, this.response);
}

// Met à jour les utilisateurs
function updateUtilisateur(obj_id) {
  display(" updating user " + obj_id);
  httpGet(PEGASS_URL+"utilisateur/"+obj_id, refreshUtilisateur);
}

// *************** LOGES

// Reçoit la liste des détails d'une activité pour les loges
function refreshActiviteLoges(event) {
  httpPost(PHANTOM_URL+"update_activite_loges", null, this.response);
}

// Met à jour les prochaines activités des loges
function updateActiviteLoges() {
  display(" updating activities for loges");
  // On ne veut que les activités pour les loges à venir
  let param_debut = "2024-06-27";
  let param_fin = "2024-08-20";
  httpGet(PEGASS_URL+"activite?libelleLike=loges&groupeAction=1&debut="+param_debut+"&fin="+param_fin, refreshActiviteLoges);
}

// Reçoit la liste des détails d'une séance des loges
function refreshSeanceLoges(event) {
  httpPost(PHANTOM_URL+"update_seance_loges", null, this.response);
}

// Met à jour les prochaines séances d'une activité des loges
function updateSeanceLoges(obj_id) {
  display(" updating seance for loges " + obj_id);
  httpGet(PEGASS_URL+"seance/"+obj_id, refreshSeanceLoges);
}

// Reçoit la liste des détails des inscriptions à une séance des loges
function refreshInscriptionLoges(event) {
  httpPost(PHANTOM_URL+"update_inscription_loges", null, this.response);
}

// Met à jour les inscriptions à une séance des loges
function updateInscriptionLoges(obj_id) {
  display(" updating subscription for loges " + obj_id);
  httpGet(PEGASS_URL+"seance/"+obj_id+"/inscription", refreshInscriptionLoges);
}

// *************** JOP 2024

// Reçoit la liste des détails d'une activité pour les jop
function refreshActiviteJop(event) {
  httpPost(PHANTOM_URL+"update_activite_jop", null, this.response);
}

// Met à jour les prochaines activités des jop
function updateActiviteJop() {
  display(" updating activities for jop");
  // On ne veut que les activités pour les jop à venir
  let param_debut = "2024-07-20";
  let param_fin = "2024-09-20";
  httpGet(PEGASS_URL+"activite?structure=83&libelleLike=JO&groupeAction=1&debut="+param_debut+"&fin="+param_fin, refreshActiviteJop);
}

// Reçoit la liste des détails d'une séance des jop
function refreshSeanceJop(event) {
  httpPost(PHANTOM_URL+"update_seance_jop", null, this.response);
}

// Met à jour les prochaines séances d'une activité des jop
function updateSeanceJop(obj_id) {
  display(" updating seance for jop " + obj_id);
  httpGet(PEGASS_URL+"seance/"+obj_id, refreshSeanceJop);
}

// Reçoit la liste des détails des inscriptions à une séance des jop
function refreshInscriptionJop(event) {
  httpPost(PHANTOM_URL+"update_inscription_jop", null, this.response);
}

// Met à jour les inscriptions à une séance des jop
function updateInscriptionJop(obj_id) {
  display(" updating subscription for jop " + obj_id);
  httpGet(PEGASS_URL+"seance/"+obj_id+"/inscription", refreshInscriptionJop);
}



// Récupère la date de dernière synchronisation
let last_update = "";
function getSynchro(event) {
  console.log("Dernière syncho: " + this.response);
  last_update = this.response;
}





// Reçoit la liste des objets à rafraichir et les rafraichit, si besoin, un par un
function refreshDatas(event) {
  delay = TEMPS;
  datas = JSON.parse(this.response);

  display("Mise à jour en cours");

  // Vérification des départements
  if (datas.departments.length > 0) {
    for (department of datas.departments) {
      setTimeout(updateDepartment, delay, department);
      delay += TEMPS;
    }
  }
  // Vérification des structures
  if (datas.structures.length > 0) {
    for (structure of datas.structures) {
      setTimeout(updateStructure, delay, structure);
      delay += TEMPS;
    }
  }
  // Vérification des activités par structure
  if (datas.activites.length > 0) {
    for (structure of datas.activites) {
      setTimeout(updateActivite, delay, structure);
      delay += TEMPS;
    }
  }
  // Vérification des séances
  if (datas.seances.length > 0) {
    for (seance of datas.seances) {
      setTimeout(updateSeance, delay, seance);
      delay += TEMPS;
    }
  }
  // Vérification des inscriptions
  if (datas.inscriptions.length > 0) {
    for (seance of datas.inscriptions) {
      setTimeout(updateInscription, delay, seance);
      delay += TEMPS;
    }
  }
  // Vérification des utilisateurs
  if (datas.utilisateurs.length > 0) {
    for (utilisateur of datas.utilisateurs) {
      setTimeout(updateUtilisateur, delay, utilisateur);
      delay += TEMPS;
    }
  }

  setTimeout(display, delay, "Mise à jour terminée");
}

// Vérifie quels objets doivent être mis à jour
function updateDatas() {
  display("Chargement des objets à mettre à jour");
  httpGet(PHANTOM_URL+"what", refreshDatas);
}



// Reçoit la liste des objets à rafraichir et les rafraichit, pour les loges
function refreshDatasLoges(event) {
  delay = TEMPS;
  datas = JSON.parse(this.response);

  display("Mise à jour en cours");

  // Vérification des activités des loges
  if (datas.activites_loges.length > 0) {
    setTimeout(updateActiviteLoges, delay);
    delay += TEMPS;
  }
  // Vérification des séances des loges
  if (datas.seances_loges.length > 0) {
    for (seance of datas.seances_loges) {
      setTimeout(updateSeanceLoges, delay, seance);
      delay += TEMPS;
    }
  }
  // Vérification des inscriptions des loges
  if (datas.inscriptions_loges.length > 0) {
    for (seance of datas.inscriptions_loges) {
      setTimeout(updateInscriptionLoges, delay, seance);
      delay += TEMPS;
    }
  }

  setTimeout(display, delay, "Mise à jour terminée");
}

// Vérifie quels objets doivent être mis à jour pour les loges
function updateDatasLoges() {
  display("Chargement des objets à mettre à jour");
  httpGet(PHANTOM_URL+"what_loges", refreshDatasLoges);
}


// Reçoit la liste des objets à rafraichir et les rafraichit, pour les JOP
function refreshDatasJop(event) {
  delay = TEMPS;
  datas = JSON.parse(this.response);

  display("Mise à jour en cours");

  // Vérification des activités des JO
  if (datas.activites_jop.length > 0) {
    setTimeout(updateActiviteJop, delay);
    delay += TEMPS;
  }
  // Vérification des séances des JO
  if (datas.seances_jop.length > 0) {
    for (seance of datas.seances_jop) {
      setTimeout(updateSeanceJop, delay, seance);
      delay += TEMPS;
    }
  }
  // Vérification des inscriptions des JO
  if (datas.inscriptions_jop.length > 0) {
    for (seance of datas.inscriptions_jop) {
      setTimeout(updateInscriptionJop, delay, seance);
      delay += TEMPS;
    }
  }

  setTimeout(display, delay, "Mise à jour terminée");
}

// Vérifie quels objets doivent être mis à jour pour les loges
function updateDatasJop() {
  display("Chargement des objets à mettre à jour");
  httpGet(PHANTOM_URL+"what_jop", refreshDatasJop);
}


// Fonction d'initialisation du script
function init() {
  // On attend que tout soit chargé pour créer le bouton proprement
  function initMenu(){
    // On récupère la date de la dernière synchro
    httpGet(PHANTOM_URL+"when", getSynchro);

    // On attend la présence de l'en-tête
    let header = document.getElementById("header");
    if (header == null){
      setTimeout(initMenu, 200);
      return;
    }
    let component_menu = header.getElementsByClassName("component-menu")[0];
    if (component_menu == null){
      setTimeout(initMenu, 200);
      return;
    }
    let main_menu = component_menu.getElementsByClassName("main-menu")[1];
    if (main_menu == null){
      setTimeout(initMenu, 200);
      return;
    }
    let menu_ul = main_menu.getElementsByTagName("ul")[0];
    if (menu_ul == null){
      setTimeout(initMenu, 200);
      return;
    }

    // Le menu est là, on crée notre sous-menu
    let menu_item = document.createElement("li");
    menu_item.id = "phantom_menu";
    menu_item.className = "item";
    let menu_link = document.createElement("a");
    menu_link.className = "item-link uppercase";
    menu_item.appendChild(menu_link);
    let menu_icon_container = document.createElement("span");
    menu_icon_container.className = "menu-icon";
    let menu_icon = document.createElement("span");
    menu_icon.className = "icon icon-refresh";
    menu_icon_container.appendChild(menu_icon);
    menu_link.appendChild(menu_icon_container);
    let menu_text = document.createElement("span");
    menu_text.innerHTML = "Tornado";
    menu_link.appendChild(menu_text);
    // On le rattache au menu existant
    menu_ul.appendChild(menu_item);
    menu_link.addEventListener("click", () => {
      updateDatas();
    });

    // Le menu est là, on crée le sous-menu des loges
    if (LOGES) {
        let menu_item = document.createElement("li");
        menu_item.id = "phantom_menu_loges";
        menu_item.className = "item";
        let menu_link = document.createElement("a");
        menu_link.className = "item-link uppercase";
        menu_item.appendChild(menu_link);
        let menu_icon_container = document.createElement("span");
        menu_icon_container.className = "menu-icon";
        let menu_icon = document.createElement("span");
        menu_icon.className = "icon icon-refresh";
        menu_icon_container.appendChild(menu_icon);
        menu_link.appendChild(menu_icon_container);
        let menu_text = document.createElement("span");
        menu_text.innerHTML = "Loges";
        menu_link.appendChild(menu_text);
        // On le rattache au menu existant
        menu_ul.appendChild(menu_item);
        menu_link.addEventListener("click", () => {
          updateDatasLoges();
        });
    }

    // Le menu est là, on crée le sous-menu des JO
    if (JO) {
        let menu_item = document.createElement("li");
        menu_item.id = "phantom_menu_jo";
        menu_item.className = "item";
        let menu_link = document.createElement("a");
        menu_link.className = "item-link uppercase";
        menu_item.appendChild(menu_link);
        let menu_icon_container = document.createElement("span");
        menu_icon_container.className = "menu-icon";
        let menu_icon = document.createElement("span");
        menu_icon.className = "icon icon-refresh";
        menu_icon_container.appendChild(menu_icon);
        menu_link.appendChild(menu_icon_container);
        let menu_text = document.createElement("span");
        menu_text.innerHTML = "JOP 2024";
        menu_link.appendChild(menu_text);
        // On le rattache au menu existant
        menu_ul.appendChild(menu_item);
        menu_link.addEventListener("click", () => {
          updateDatasJop();
        });
    }

    // Le menu est là, on crée notre bandeau d'information
    let logo = document.getElementById("logo");
    let bandeau = document.createElement("div");
    bandeau.className = "center";
    let bandeau_text = document.createElement("p");
    bandeau_text.className = "flash-message flash-message-data flash-message-success";
    bandeau_text.style = "position:relative;width:50%;margin:auto";
    bandeau_text.id= "phantom_text";
    bandeau.appendChild(bandeau_text);
    logo.parentNode.insertBefore(bandeau, logo.nextSibling);

    display("V"+VERSION+" - Dernière mise à jour : "+last_update);
  }
  setTimeout(initMenu, 200);
}

init();
