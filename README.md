# Bienvenue chez Phantom

Cet outil permet de gérer les données des postes de secours du 78 via les appels du
script utilisateur [phantom](/static/phantom.user.js).

## Installation

Il est nécessaire d'installer le plugin "ViolentMonkey" sur votre navigateur, puis
simplement d'installer le script en cliquant sur son lien.

# Utilisation

Une fois le script installé, il suffit de se connecter à votre compte sur Pegass. Si
tout s'est déroulé correctement, vous devriez voir deux choses sur Pegass :

* Un gros bloc vert en haut de l'écran qui vous indique que le script est bien installé
    et semble fonctionner. Vous avez également accès à sa version au cas où.
* Un nouveau bouton de menu (appelé Tornado) qui vous permet alors de lancer la
    synchronisation des données.

# Synchornisation des données

La synchronisation des données permet de récupérer l'ensemble des données nécessaires au
bon affichage de Tornado. Si vous synchronisez moins de 24 heures après une précédente
synchronisation, il ne se passera rien pour protéger le tout.

En général, on n'est pas à une journée pour faire apparaitre un nouveau poste (car dans
le futur), ni les nouvelles inscriptions (Tornado est surtout un outil de consultation,
pas de gestion).
