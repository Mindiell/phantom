# encoding: utf-8

import os

from dotenv import load_dotenv


load_dotenv()

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
DEBUG = os.environ.get("DEBUG", False)
SECRET_KEY = os.environ.get("SECRET_KEY", "Choose a secret key")

DATA_PATH = os.environ.get("DATA_PATH", "datas/")
